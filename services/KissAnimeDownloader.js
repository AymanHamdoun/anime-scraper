const puppeteer = require('puppeteer-extra');

// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

// Add adblocker plugin to block all ads and trackers (saves bandwidth)
const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
puppeteer.use(AdblockerPlugin({ blockTrackers: true }))

const userAgent = require('user-agents');


const LOGIN_URL = "https://kissanime.com.ru/Login/";
const SAMPLE_EPISODE_URL = "https://kissanime.com.ru/Anime/One-Piece.18235/Episode-999?id=173966";

const KISSANIME_USERNAME = "foraccount@yahoo.com";
const KISSANIME_PASSWORD = "AlphaCentauri123";

const KissAnimeInfo = {
    loginUserNameSelector: '#username',
    loginPasswordSelector: '#password',
    loginButtonSelector: '#btnSubmit',
    
    episodeSelectorToWaitFor: '#div_donoat_link',
    episodeSelectorToClick: '#div_donoat_link a',
};

const openKissAnime = async () => {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.setUserAgent(userAgent.toString())
    
    console.log("GO TO LOGIN");
    await loginToKissAnime(page);
    
    console.log("GO TO DOWNLOAD");
    await downloadEpisode(page, SAMPLE_EPISODE_URL);
    // await browser.close();
}

const loginToKissAnime = async (page) => {
    // go to login page: 
    await page.goto(LOGIN_URL);
    await page.waitForSelector(KissAnimeInfo.loginUserNameSelector);
    // fill in login info: 
    await page.$eval(KissAnimeInfo.loginUserNameSelector, (el, KISSANIME_USERNAME) => {el.value = KISSANIME_USERNAME}, KISSANIME_USERNAME);  
    await page.$eval(KissAnimeInfo.loginPasswordSelector, (el, KISSANIME_PASSWORD) => {el.value = KISSANIME_PASSWORD}, KISSANIME_PASSWORD);  
    // click login button: 
    await page.$eval(KissAnimeInfo.loginButtonSelector, (el) => {el.click()});  
}

const downloadEpisode = async (page, url) => {
    await page.goto(url);
    await page.waitForSelector(KissAnimeInfo.episodeSelectorToWaitFor);
    
    await page.$eval(KissAnimeInfo.episodeSelectorToClick, (el) => {
        return el.click();
    });  
}

openKissAnime().catch(error => {
    console.log(error);
});