const puppeteer = require('puppeteer-extra');

// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

// Add adblocker plugin to block all ads and trackers (saves bandwidth)
const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
puppeteer.use(AdblockerPlugin({ blockTrackers: true }))

const SAMPLE_EPISODE_URL = "https://www2.animeshow.tv/one-piece-episode-997/";

const openAnimeShow = async () => {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();

    await page.goto(SAMPLE_EPISODE_URL);
    await page.waitForSelector(".jw-icon.jw-button-image.jw-button-color.jw-reset");
    await page.$eval(".jw-icon.jw-button-image.jw-button-color.jw-reset", (el) => {el.click()});  
}

const downloadEpisode = async (page) => {
    let url = "https://gogoplay1.com/download?id=MTczODI3&refer=https://www2.animeshow.tv/&ch=d41d8cd98f00b204e9800998ecf8427e";
    await page.goto(url);
    await page.waitForSelector('#main a');

    const linkElements = await page.$$('a');  
    
    for (var i = 0; i < linkElements.length; i++) {
        let hrefValue = await (await linkElements[i].getProperty('href')).jsonValue();
        console.log(hrefValue);
        if (i == 1) {
            linkElements[i].click();
        }
    }
}

openAnimeShow().catch(error => {
    console.log(error);
});